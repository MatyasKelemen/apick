"use strict";

const axios = require("axios");
const expect = require("chai").expect;

const helpers = require("../../utils/helpers");
const testData = require("../../data/getSessionId");

describe("Authentication", () => {
    testData.forEach(test => {
        describe("Create session with request token", () => {
            let response;
            beforeEach(async () => {
                if (test.editRequest) {
                    const requestToken = await helpers.getRequestToken();
                    test.input.data.request_token = await helpers.approveRequestToken(requestToken);
                }
            });

            afterEach(async () => {
                if (response.data.session_id) {
                    await helpers.removeSessionId(response.data.session_id);
                }
            });

            it(`${test.testName}`, async () => {
                response = await axios(test.input);
                expect(response.status).to.be.equal(test.expected.status);
                expect(response.data).to.be.an.instanceof(Object);
                expect(response.data).to.have.keys(test.expected.response);
                if (test.checkDeepEquality) {
                    expect(response.data).to.be.eql(test.expected.response);
                }
            });
        });
    });
});
