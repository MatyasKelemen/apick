"use strict";

const axios = require("axios");
const expect = require("chai").expect;

const helpers = require("../../utils/helpers");
const testData = require("../../data/removeSessionId");

describe("Authentication", () => {
    testData.forEach(test => {
        describe("Logout from a session", () => {
            let sessionId;
            beforeEach(async () => {
                if (test.editRequest) {
                    const requestToken = await helpers.getRequestToken();
                    const approvedToken = await helpers.approveRequestToken(requestToken);
                    sessionId = await helpers.getSessionId(approvedToken);
                    test.input.data.session_id = sessionId;
                }
            });

            afterEach(async () => {
                if (sessionId) {
                    await helpers.removeSessionId(sessionId);
                }
            });

            it(`${test.testName}`, async () => {
                const response = await axios(test.input);
                expect(response.status).to.be.equal(test.expected.status);
                expect(response.data).to.be.an.instanceof(Object);
                expect(response.data).to.have.keys(test.expected.response);
                if (test.checkDeepEquality) {
                    expect(response.data).to.be.eql(test.expected.response);
                }
            });
        });
    });
});
