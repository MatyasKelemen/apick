"use strict";

const axios = require("axios");
const expect = require("chai").expect;

const testData = require("../../data/getRequestToken");

describe("Authentication", () => {
    testData.forEach(test => {
        describe("Create request token", () => {
            it(`${test.testName}`, async () => {
                const response = await axios(test.input);
                expect(response.status).to.be.equal(test.expected.status);
                expect(response.data).to.be.an.instanceof(Object);
                expect(response.data).to.have.keys(test.expected.response);
                if (test.checkDeepEquality) {
                    expect(response.data).to.be.eql(test.expected.response);
                }
            });
        });
    });
});
