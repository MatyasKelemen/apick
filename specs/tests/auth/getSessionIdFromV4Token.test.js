"use strict";

const axios = require("axios");
const expect = require("chai").expect;

const testData = require("../../data/getSessionIdFromV4Token");

describe("Authentication", () => {
    testData.forEach(test => {
        describe("Create session from a V4 access token", () => {
            it(`${test.testName}`, async () => {
                const response = await axios(test.input);
                expect(response.status).to.be.equal(test.expected.status);
                expect(response.data).to.be.an.instanceof(Object);
                expect(response.data).to.be.eql(test.expected.response);
            });
        });
    });
});
