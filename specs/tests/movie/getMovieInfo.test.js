"use strict";

const axios = require("axios");
const expect = require("chai").expect;
const { StatusCodes } = require("http-status-codes");

const URL = require("../../utils/endpoints");
const helpers = require("../../utils/helpers");

describe("Movies", () => {
    describe("Get movie info", () => {
        let request; let movieId;
        before(() => {
            request = axios.create({
                baseURL: URL.base,
                params: {
                    api_key: process.env.TMDB_API_KEY,
                },
                validateStatus: false,
            });
        });

        beforeEach(async () => {
            movieId = await helpers.getRandomMovieId();
        });

        it("should provide data about a given movie", async () => {
            const response = await request.get(`${URL.movie}${movieId}`);
            expect(response.status).to.be.equal(StatusCodes.OK);
            expect(response.data).to.be.an.instanceof(Object);
            expect(response.data).to.have.property("original_title").that.is.a("string");
        });

        it("shouldn't provide data in case of an invalid API key", async () => {
            const response = await request.get(`${URL.movie}${movieId}`, {
                params: {
                    api_key: process.env.TMDB_INVALID_API_KEY,
                },
            });
            expect(response.status).to.be.equal(StatusCodes.UNAUTHORIZED);
            expect(response.data).to.be.an.instanceof(Object);
            expect(response.data).to.be.eql({
                "status_code": 7,
                "status_message": "Invalid API key: You must be granted a valid key.",
                "success": false,
            });
        });

        it("shouldn't provide data in case of an invalid URL", async () => {
            const response = await request.get(`${URL.invalidMovie}${movieId}`);
            expect(response.status).to.be.equal(StatusCodes.NOT_FOUND);
            expect(response.data).to.be.an.instanceof(Object);
            expect(response.data).to.be.eql({
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            });
        });

        it("shouldn't provide data in case of missing movie Id", async () => {
            const response = await request.get(URL.movie);
            expect(response.status).to.be.equal(StatusCodes.NOT_FOUND);
            expect(response.data).to.be.an.instanceof(Object);
            expect(response.data).to.be.eql({
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            });
        });

        it("shouldn't provide data in case of an invalid movie Id", async () => {
            const response = await request.get(`${URL.movie}000000`);
            expect(response.status).to.be.equal(StatusCodes.NOT_FOUND);
            expect(response.data).to.be.an.instanceof(Object);
            expect(response.data).to.be.eql({
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            });
        });
    });
});
