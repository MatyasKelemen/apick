"use strict";

const axios = require("axios");
const expect = require("chai").expect;
const { StatusCodes } = require("http-status-codes");

const URL = require("../../utils/endpoints");
const helpers = require("../../utils/helpers");

describe("Movies", () => {
    describe("Rate a movie", () => {
        let request; let movieId; let sessionId;
        beforeEach(async () => {
            const requestToken = await helpers.getRequestToken();
            const approvedToken = await helpers.approveRequestToken(requestToken);
            sessionId = await helpers.getSessionId(approvedToken);
            movieId = await helpers.getRandomMovieId();

            request = axios.create({
                baseURL: `${URL.base}${URL.movie}`,
                params: {
                    api_key: process.env.TMDB_API_KEY,
                    session_id: sessionId,
                },
                validateStatus: false,
            });
        });

        afterEach(async () => {
            try {
                await helpers.removeMovieRate(movieId, sessionId);
            } catch (error) {
                return error;
            }
            await helpers.removeSessionId(sessionId);
        });

        it("should be able to rate a given movie", async () => {
            const response = await request.post(
                `${movieId}${URL.rateMovie}`,
                {
                    value: 0.5,
                },
            );
            expect(response.status).to.be.equal(StatusCodes.CREATED);
            expect(response.data).to.be.an.instanceof(Object);
            expect(response.data).to.be.eql({
                "success": true,
                "status_code": 1,
                "status_message": "Success.",
            });
        });

        it("shouldn't be able to rate a movie in case of an invalid URL", async () => {
            const response = await request.post(
                `${movieId}${URL.invalidRateMovie}`,
                {
                    value: 10,
                },
            );
            expect(response.status).to.be.equal(StatusCodes.NOT_FOUND);
            expect(response.data).to.be.an.instanceof(Object);
            expect(response.data).to.be.eql({
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            });
        });

        it("shouldn't be able to rate a movie in case of missing movie Id", async () => {
            const response = await request.post(
                `${URL.rateMovie}`,
                {
                    value: 10,
                },
            );
            expect(response.status).to.be.equal(StatusCodes.NOT_FOUND);
            expect(response.data).to.be.an.instanceof(Object);
            expect(response.data).to.be.eql({
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            });
        });

        it("shouldn't be able to rate a movie in case of an invalid movie Id", async () => {
            const response = await request.post(
                `000000${URL.rateMovie}`,
                {
                    value: 10,
                },
            );
            expect(response.status).to.be.equal(StatusCodes.NOT_FOUND);
            expect(response.data).to.be.an.instanceof(Object);
            expect(response.data).to.be.eql({
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            });
        });

        it("shouldn't be able to rate a movie in case of an invalid API key", async () => {
            const response = await request.post(
                `${movieId}${URL.rateMovie}`,
                {
                    value: 10,
                },
                {
                    params: {
                        api_key: process.env.TMDB_INVALID_API_KEY,
                    },
                },
            );
            expect(response.status).to.be.equal(StatusCodes.UNAUTHORIZED);
            expect(response.data).to.be.an.instanceof(Object);
            expect(response.data).to.be.eql({
                "status_code": 7,
                "status_message": "Invalid API key: You must be granted a valid key.",
                "success": false,
            });
        });

        it("shouldn't be able to rate a movie in case of a missing session Id", async () => {
            const response = await request.post(
                `${movieId}${URL.rateMovie}`,
                {
                    value: 10,
                },
                {
                    params: {
                        session_id: "",
                    },
                },
            );
            expect(response.status).to.be.equal(StatusCodes.UNAUTHORIZED);
            expect(response.data).to.be.an.instanceof(Object);
            expect(response.data).to.be.eql({
                "success": false,
                "status_code": 3,
                "status_message": "Authentication failed: You do not have permissions to access the service.",
            });
        });

        it("shouldn't be able to rate a movie in case of an invalid session Id", async () => {
            const response = await request.post(
                `${movieId}${URL.rateMovie}`,
                {
                    value: 10,
                },
                {
                    params: {
                        session_id: process.env.TMDB_INVALID_SESSION_ID,
                    },
                },
            );
            expect(response.status).to.be.equal(StatusCodes.UNAUTHORIZED);
            expect(response.data).to.be.an.instanceof(Object);
            expect(response.data).to.be.eql({
                "success": false,
                "status_code": 3,
                "status_message": "Authentication failed: You do not have permissions to access the service.",
            });
        });
    });
});
