"use strict";

const axios = require("axios");
const URL = require("./endpoints");
const { StatusCodes } = require("http-status-codes");

/**
 * Create a request token
 *
 * @param {Object} [axiosRequestConfig]
 * @returns {Promise<string>}
 */
async function getRequestToken(conf = {
    method: "GET",
    baseURL: URL.base,
    url: URL.newToken,
    params: {
        api_key: process.env.TMDB_API_KEY,
    },
}) {
    await new Promise(resolve => {
        // Additional wait to avoid identical tokens
        setTimeout(resolve, 750);
    });
    return axios(conf).then(response => {
        if (response.status === StatusCodes.OK) {
            return response.data.request_token;
        }
    });
}

/**
 * Approve a request token with login
 *
 * @param {string} token
 * @param {Object} [axiosRequestConfig]
 * @returns {Promise<string>}
 */
function approveRequestToken(token, conf = {
    method: "POST",
    baseURL: URL.base,
    url: URL.newSessionWithLogin,
    params: {
        api_key: process.env.TMDB_API_KEY,
    },
    data: {
        username: process.env.TMDB_USERNAME,
        password: process.env.TMDB_PASSWORD,
        request_token: token,
    },
}) {
    return axios(conf).then(response => {
        if (response.status === StatusCodes.OK) {
            return response.data.request_token;
        }
    });
}

/**
 * Create a session with an approved request token
 *
 * @param {string} approvedRequestToken
 * @param {Object} [axiosRequestConfig]
 * @returns {Promise<string>}
 */
function getSessionId(approvedRequestToken, conf = {
    method: "POST",
    baseURL: URL.base,
    url: URL.newSession,
    params: {
        api_key: process.env.TMDB_API_KEY,
    },
    data: {
        request_token: approvedRequestToken,
    },
}) {
    return axios(conf).then(response => {
        if (response.status === StatusCodes.OK) {
            return response.data.session_id;
        }
    });
}

/**
 * Create a session with an approved V4 access token
 *
 * @param {string} approvedAccessToken
 * @param {Object} [axiosRequestConfig]
 * @returns {Promise<string>}
 */
function getSessionIdFromV4Token(approvedAccessToken, conf = {
    method: "POST",
    baseURL: URL.base,
    url: URL.newSessionFromV4Token,
    params: {
        api_key: process.env.TMDB_API_KEY,
    },
    data: {
        access_token: approvedAccessToken,
    },
}) {
    return axios(conf).then(response => {
        if (response.status === StatusCodes.OK) {
            return response.data.session_id;
        }
    });
}

/**
 * Create a Guest session
 *
 * @param {Object} [axiosRequestConfig]
 * @returns {Promise<string>}
 */
function getGuestSessionId(conf = {
    method: "GET",
    baseURL: URL.base,
    url: URL.newGuestSession,
    params: {
        api_key: process.env.TMDB_API_KEY,
    },
}) {
    return axios(conf).then(response => {
        if (response.status === StatusCodes.OK) {
            return response.data.guest_session_id;
        }
    });
}

/**
 * Logout from a session
 *
 * @param {string} sessionId
 * @param {Object} [axiosRequestConfig]
 * @returns {Promise<boolean>}
 */
function removeSessionId(sessionId, conf = {
    method: "DELETE",
    baseURL: URL.base,
    url: URL.removeSession,
    params: {
        api_key: process.env.TMDB_API_KEY,
    },
    data: {
        session_id: sessionId,
    },
}) {
    return axios(conf).then(response => {
        if (response.status === StatusCodes.OK) {
            return response.data.success;
        }
    }).catch(error => error);
}

/**
 * Provide a valid movie Id
 *
 * @param {Object} [axiosRequestConfig]
 * @returns {Promise<number>}
 */
function getRandomMovieId(conf = {
    method: "GET",
    baseURL: URL.base,
    url: URL.popular,
    params: {
        api_key: process.env.TMDB_API_KEY,
    },
}) {
    return axios(conf).then(response => {
        if (Array.isArray(response.data.results) && response.data.results.length > 0) {
            const movies = response.data.results;
            const randomMovie = movies[Math.floor(Math.random() * movies.length)];
            return randomMovie.id;
        }
    });
}

/**
 * Get information about a movie
 *
 * @param {number} movieId
 * @param {Object} [axiosRequestConfig]
 * @returns {Promise<string>}
 */
function getMovieInfo(movieId, conf = {
    method: "GET",
    baseURL: URL.base,
    url: `${URL.movie}${movieId}`,
    params: {
        api_key: process.env.TMDB_API_KEY,
    },
}) {
    return axios(conf).then(response => {
        if (response.status === StatusCodes.OK) {
            return response.data;
        }
    });
}

/**
 * Rate a movie
 *
 * @param {number} movieId
 * @param {number} rate
 * @param {number} sessionId
 * @param {Object} [axiosRequestConfig]
 * @returns {Promise<string>}
 */
function rateMovie(movieId, rate, sessionId, conf = {
    method: "POST",
    baseURL: URL.base,
    url: `${URL.movie}${movieId}${URL.rateMovie}`,
    params: {
        api_key: process.env.TMDB_API_KEY,
        session_id: sessionId,
    },
    data: {
        value: rate,
    },
}) {
    return axios(conf).then(response => {
        if (response.status === StatusCodes.CREATED) {
            return response.data.status_message;
        }
    });
}

/**
 * Remove the rating of a movie
 *
 * @param {number} movieId
 * @param {number} sessionId
 * @param {Object} [axiosRequestConfig]
 * @returns {Promise<string>}
 */
function removeMovieRate(movieId, sessionId, conf = {
    method: "DELETE",
    baseURL: URL.base,
    url: `${URL.movie}${movieId}${URL.rateMovie}`,
    params: {
        api_key: process.env.TMDB_API_KEY,
        session_id: sessionId,
    },
}) {
    return axios(conf).then(response => {
        if (response.status === StatusCodes.OK) {
            return response.data.status_message;
        }
    });
}

module.exports = {
    getRequestToken,
    approveRequestToken,
    getSessionId,
    getSessionIdFromV4Token,
    getGuestSessionId,
    removeSessionId,
    getRandomMovieId,
    getMovieInfo,
    rateMovie,
    removeMovieRate,
};
