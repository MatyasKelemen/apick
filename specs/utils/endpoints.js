"use strict";

module.exports = {
    base: "https://api.themoviedb.org/3/",
    invalidMovie: "moovie/",
    invalidNewGuestSession: "authentication/guest_session/neew",
    invalidNewSession: "authentication/session/neew",
    invalidNewSessionFromV4Token: "authentication/sesion/convert/4",
    invalidNewSessionWithLogin: "authentication/token/validate_with_logn",
    invalidNewToken: "authentication/token/neew",
    invalidRateMovie: "/ratin",
    invalidRemoveSession: "authentication/sesion",
    movie: "movie/",
    newGuestSession: "authentication/guest_session/new",
    newSession: "authentication/session/new",
    newSessionFromV4Token: "authentication/session/convert/4",
    newSessionWithLogin: "authentication/token/validate_with_login",
    newToken: "authentication/token/new",
    popular: "movie/popular",
    rateMovie: "/rating",
    removeSession: "authentication/session",
};
