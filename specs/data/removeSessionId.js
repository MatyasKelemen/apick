"use strict";

const URL = require("../utils/endpoints");
const { StatusCodes } = require("http-status-codes");

module.exports = [
    {
        testName: "should logout from a session",
        input: {
            method: "DELETE",
            baseURL: URL.base,
            url: URL.removeSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                session_id: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.OK,
            response: {
                "success": true,
            },
        },
        checkDeepEquality: true,
        editRequest: true,
    },
    {
        testName: "shouldn't logout from a session in case of an invalid API key",
        input: {
            method: "DELETE",
            baseURL: URL.base,
            url: URL.removeSession,
            params: {
                api_key: process.env.TMDB_INVALID_API_KEY,
            },
            data: {
                session_id: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.UNAUTHORIZED,
            response: {
                "status_code": 7,
                "status_message": "Invalid API key: You must be granted a valid key.",
                "success": false,
            },
        },
        checkDeepEquality: true,
        editRequest: true,
    },
    {
        testName: "shouldn't logout from a session in case of an invalid session ID",
        input: {
            method: "DELETE",
            baseURL: URL.base,
            url: URL.removeSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                session_id: process.env.TMDB_INVALID_SESSION_ID,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.NOT_FOUND,
            response: {
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't logout from a session in case of an expired session ID",
        input: {
            method: "DELETE",
            baseURL: URL.base,
            url: URL.removeSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                session_id: process.env.TMDB_EXPIRED_SESSION_ID,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.NOT_FOUND,
            response: {
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't logout from a session in case of an invalid URL",
        input: {
            method: "DELETE",
            baseURL: URL.base,
            url: URL.invalidRemoveSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                session_id: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.NOT_FOUND,
            response: {
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            },
        },
        checkDeepEquality: true,
        editRequest: true,
    },
    {
        testName: "shouldn't logout from a session in case of an empty request body",
        input: {
            method: "DELETE",
            baseURL: URL.base,
            url: URL.removeSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.BAD_REQUEST,
            response: {
                "success": false,
                "status_code": 5,
                "status_message": "Invalid parameters: Your request parameters are incorrect.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't logout from a session with invalid body property keys",
        input: {
            method: "DELETE",
            baseURL: URL.base,
            url: URL.removeSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                sesion_id: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.BAD_REQUEST,
            response: {
                "success": false,
                "status_code": 5,
                "status_message": "Invalid parameters: Your request parameters are incorrect.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
];
