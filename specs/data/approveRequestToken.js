"use strict";

const URL = require("../utils/endpoints");
const { StatusCodes } = require("http-status-codes");

module.exports = [
    {
        testName: "should approve a valid request token",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionWithLogin,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                username: process.env.TMDB_USERNAME,
                password: process.env.TMDB_PASSWORD,
                request_token: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.OK,
            response: {
                "success": true,
                "expires_at": "",
                "request_token": "",
            },
        },
        checkDeepEquality: false,
        editRequest: true,
    },
    {
        testName: "shouldn't return a token if the request token was an empty string",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionWithLogin,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                username: process.env.TMDB_USERNAME,
                password: process.env.TMDB_PASSWORD,
                request_token: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.OK,
            response:  {
                "success": true,
                "expires_at": "",
                "request_token": "",
            },
        },
        checkDeepEquality: false,
        editRequest: false,
    },
    {
        testName: "shouldn't approve a token in case of an invalid API key",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionWithLogin,
            params: {
                api_key: process.env.TMDB_INVALID_API_KEY,
            },
            data: {
                username: process.env.TMDB_USERNAME,
                password: process.env.TMDB_PASSWORD,
                request_token: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.UNAUTHORIZED,
            response: {
                "status_code": 7,
                "status_message": "Invalid API key: You must be granted a valid key.",
                "success": false,
            },
        },
        checkDeepEquality: true,
        editRequest: true,
    },
    {
        testName: "shouldn't approve a token in case of an invalid request token",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionWithLogin,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                username: process.env.TMDB_USERNAME,
                password: process.env.TMDB_PASSWORD,
                request_token: process.env.TMDB_USED_REQUEST_TOKEN,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.UNAUTHORIZED,
            response:  {
                "success": false,
                "status_code": 33,
                "status_message": "Invalid request token: The request token is either expired or invalid.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't approve a token in case of invalid credentials",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionWithLogin,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                username: process.env.TMDB_USERNAME,
                password: process.env.TMDB_INVALID_PASSWORD,
                request_token: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.UNAUTHORIZED,
            response: {
                "success": false,
                "status_code": 30,
                "status_message": "Invalid username and/or password: You did not provide a valid login.",
            },
        },
        checkDeepEquality: true,
        editRequest: true,
    },
    {
        testName: "shouldn't approve a token in case of invalid URL",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.invalidNewSessionWithLogin,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                username: process.env.TMDB_USERNAME,
                password: process.env.TMDB_PASSWORD,
                request_token: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.NOT_FOUND,
            response: {
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            },
        },
        checkDeepEquality: true,
        editRequest: true,
    },
    {
        testName: "shouldn't process approve requests without body",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionWithLogin,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.BAD_REQUEST,
            response: {
                "success": false,
                "status_code": 5,
                "status_message": "Invalid parameters: Your request parameters are incorrect.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't process approve requests with invalid body property keys",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionWithLogin,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                usename: process.env.TMDB_USERNAME,
                pasword: process.env.TMDB_PASSWORD,
                reuest_token: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.BAD_REQUEST,
            response: {
                "success": false,
                "status_code": 5,
                "status_message": "Invalid parameters: Your request parameters are incorrect.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't process approve requests with invalid Username and Password body property keys",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionWithLogin,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                usename: process.env.TMDB_USERNAME,
                pasword: process.env.TMDB_PASSWORD,
                request_token: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.BAD_REQUEST,
            response: {
                "success": false,
                "status_code": 26,
                "status_message": "You must provide a username and password.",
            },
        },
        checkDeepEquality: true,
        editRequest: true,
    },
    {
        testName: "shouldn't process approve requests with empty credentials",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionWithLogin,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                username: "",
                password: "",
                request_token: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.BAD_REQUEST,
            response: {
                "success": false,
                "status_code": 26,
                "status_message": "You must provide a username and password.",
            },
        },
        checkDeepEquality: true,
        editRequest: true,
    },
    {
        testName: "shouldn't process approve requests without request_token property in the body",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionWithLogin,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                username: process.env.TMDB_USERNAME,
                password: process.env.TMDB_PASSWORD,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.BAD_REQUEST,
            response: {
                "success": false,
                "status_code": 5,
                "status_message": "Invalid parameters: Your request parameters are incorrect.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
];

