"use strict";

const URL = require("../utils/endpoints");
const { StatusCodes } = require("http-status-codes");

module.exports = [
    {
        testName: "shouldn't convert session with an unapproved V4 access token",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionFromV4Token,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                access_token: process.env.TMDB_READ_ACCESS_TOKEN,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.UNAUTHORIZED,
            response: {
                "success": false,
                "failure": true,
                "status_code": 36,
                "status_message": "This token hasn't been granted write permission by the user.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't convert session in case of an ivalid API key",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionFromV4Token,
            params: {
                api_key: process.env.TMDB_INVALID_API_KEY,
            },
            data: {
                access_token: process.env.TMDB_READ_ACCESS_TOKEN,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.UNAUTHORIZED,
            response: {
                "status_code": 7,
                "status_message": "Invalid API key: You must be granted a valid key.",
                "success": false,
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't convert session in case of an ivalid V4 access token",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionFromV4Token,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                access_token: process.env.TMDB_INVALID_READ_ACCESS_TOKEN,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.UNAUTHORIZED,
            response: {
                "success": false,
                "failure": true,
                "status_code": 35,
                "status_message": "Invalid token.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't convert session in case of an ivalid URL",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.invalidNewSessionFromV4Token,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                access_token: process.env.TMDB_READ_ACCESS_TOKEN,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.NOT_FOUND,
            response: {
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't convert session in case of an empty request body",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionFromV4Token,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.BAD_REQUEST,
            response: {
                "success": false,
                "status_code": 5,
                "status_message": "Invalid parameters: Your request parameters are incorrect.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't convert session with invalid body property keys",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSessionFromV4Token,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                acess_token: process.env.TMDB_READ_ACCESS_TOKEN,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.BAD_REQUEST,
            response: {
                "success": false,
                "status_code": 5,
                "status_message": "Invalid parameters: Your request parameters are incorrect.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
];
