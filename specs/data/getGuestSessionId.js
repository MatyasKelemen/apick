"use strict";

const URL = require("../utils/endpoints");
const { StatusCodes } = require("http-status-codes");

module.exports = [
    {
        testName: "should create a guest session",
        input: {
            method: "GET",
            baseURL: URL.base,
            url: URL.newGuestSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.OK,
            response: {
                "success": true,
                "guest_session_id": "",
                "expires_at": "",
            },
        },
        checkDeepEquality: false,
        editRequest: false,
    },
    {
        testName: "shouldn't create a guest session in case of an invalid API key",
        input: {
            method: "GET",
            baseURL: URL.base,
            url: URL.newGuestSession,
            params: {
                api_key: process.env.TMDB_INVALID_API_KEY,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.UNAUTHORIZED,
            response: {
                "status_code": 7,
                "status_message": "Invalid API key: You must be granted a valid key.",
                "success": false,
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't create a guest session in case of an invalid URL",
        input: {
            method: "GET",
            baseURL: URL.base,
            url: URL.invalidNewGuestSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.NOT_FOUND,
            response: {
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
];
