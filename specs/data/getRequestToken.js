"use strict";

const URL = require("../utils/endpoints");
const { StatusCodes } = require("http-status-codes");

module.exports = [
    {
        testName: "should return a request token",
        input: {
            method: "GET",
            baseURL: URL.base,
            url: URL.newToken,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.OK,
            response: {
                "success": true,
                "expires_at": "",
                "request_token": "",
            },
        },
        checkDeepEquality: false,
        editRequest: false,
    },
    {
        testName: "shouldn't return a request token in case of an invalid API key",
        input: {
            method: "GET",
            baseURL: URL.base,
            url: URL.newToken,
            params: {
                api_key: process.env.TMDB_INVALID_API_KEY,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.UNAUTHORIZED,
            response: {
                "status_code": 7,
                "status_message": "Invalid API key: You must be granted a valid key.",
                "success": false,
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't return a request token in case of an invalid URL",
        input: {
            method: "GET",
            baseURL: URL.base,
            url: URL.invalidNewToken,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.NOT_FOUND,
            response: {
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },

];
