"use strict";

const URL = require("../utils/endpoints");
const { StatusCodes } = require("http-status-codes");

module.exports = [
    {
        testName: "should create a session with an approved request token",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                request_token: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.OK,
            response: {
                "success": true,
                "session_id": "",
            },
        },
        checkDeepEquality: false,
        editRequest: true,
    },
    {
        testName: "shouldn't create a session in case of an invalid API key",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSession,
            params: {
                api_key: process.env.TMDB_INVALID_API_KEY,
            },
            data: {
                request_token: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.UNAUTHORIZED,
            response: {
                "status_code": 7,
                "status_message": "Invalid API key: You must be granted a valid key.",
                "success": false,
            },
        },
        checkDeepEquality: true,
        editRequest: true,
    },
    {
        testName: "shouldn't create a session with an already used request token",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                request_token: process.env.TMDB_USED_REQUEST_TOKEN,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.UNAUTHORIZED,
            response: {
                "success": false,
                "failure": true,
                "status_code": 17,
                "status_message": "Session denied.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't create a session with an invalid request token",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                request_token: process.env.TMDB_INVALID_REQUEST_TOKEN,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.UNAUTHORIZED,
            response: {
                "success": false,
                "failure": true,
                "status_code": 17,
                "status_message": "Session denied.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't create a session with invalid URL",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.invalidNewSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                request_token: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.NOT_FOUND,
            response: {
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            },
        },
        checkDeepEquality: true,
        editRequest: true,
    },
    {
        testName: "shouldn't create a session with invalid body property keys",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            data: {
                reuest_token: "",
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.NOT_FOUND,
            response: {
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
    {
        testName: "shouldn't create a session with empty request body",
        input: {
            method: "POST",
            baseURL: URL.base,
            url: URL.newSession,
            params: {
                api_key: process.env.TMDB_API_KEY,
            },
            validateStatus: false,
        },
        expected: {
            status: StatusCodes.NOT_FOUND,
            response: {
                "success": false,
                "status_code": 34,
                "status_message": "The resource you requested could not be found.",
            },
        },
        checkDeepEquality: true,
        editRequest: false,
    },
];
