# apick
Basic API test automation framework for TMDB.

### Installing Dev. requirements:
- Prerequisites: [NodeJS 16](https://nodejs.org/en/), [Git](https://git-scm.com/)

### Installation of the Dev. Environment:
Write the followings into a terminal standing in the project folder:
    
    $> yarn [install]

### Setup of the Dev. Environment:
Register for an [API key](https://developers.themoviedb.org/3/getting-started/introduction). \
In the root folder create an .env file based on the .env_sample. \
In case of a CI run the environment variables must be set by the host (Environment secrets).
        
### Running tests locally
In the root folder run a command like:

    $> yarn test
    $> yarn test:report

### Running tests on remote

    $> yarn test:ci
